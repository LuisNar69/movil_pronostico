import 'dart:async';
import 'dart:developer';

import 'package:app_pronostico/controls/servicio_back/FacadeService.dart';
import 'package:app_pronostico/models/sensor.dart';
import 'package:flutter/material.dart';

class PrincipalView extends StatefulWidget {
  const PrincipalView({Key? key}) : super(key: key);

  @override
  _PrincipalViewState createState() => _PrincipalViewState();
}

class _PrincipalViewState extends State<PrincipalView> {
  Future<void> _refresh() async {
    await _listar();
    await _listarP();
  }

  Future<Sensor> _listar() async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarDato();

      if (value.code == 200) {
        return value.data;
      } else {
        if (value.code != 200) {
          Navigator.pushNamed(context, '/principal');
        }
      }
    } catch (e) {
      log("Error al cargar dato: $e");
    }
    return Sensor();
  }

  Future<List<dynamic>?> _listarP() async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarPronosticos();
      return value;
    } catch (e) {
      log("Error al cargar pronosticos: $e");
      return null; // Devuelve null en caso de error
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.transparent,
        appBarTheme: const AppBarTheme(
          backgroundColor: Color.fromARGB(19, 0, 0, 0),
          elevation: 0,
        ),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Pronósticos UNL',
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Color.fromARGB(186, 0, 0, 0),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/logo.png"),
                alignment:
                    Alignment.centerLeft, // Alinea la imagen a la izquierda
              ),
            ),
          ),
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/fondo1.jpg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                  Color.fromARGB(49, 78, 78, 78).withOpacity(0.5),
                  BlendMode.colorBurn,
                ),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                ),
                SizedBox(
                  width: 200,
                  height: 200,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(color: Colors.black, width: 1.0),
                    ),
                    color: Color.fromARGB(255, 255, 241, 114),
                    child: Center(
                      child: FutureBuilder<Sensor?>(
                        future: _listar(),
                        builder: (BuildContext context,
                            AsyncSnapshot<Sensor?> snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasData && snapshot.data != null) {
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text(
                                    'Temperatura:',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromARGB(255, 0, 0, 0),
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Text(
                                    snapshot.data!.dato.toString() + " °C",
                                    style: const TextStyle(
                                      fontSize: 30,
                                      color: Color.fromARGB(255, 0, 0, 0),
                                    ),
                                  ),
                                ],
                              );
                            } else {
                              return Container();
                            }
                          } else {
                            return Center(child: CircularProgressIndicator());
                          }
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 20,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                ),
                const Text(
                  'Predicciones:',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 255, 255, 255),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: FutureBuilder<List<dynamic>?>(
                      future: _listarP(),
                      initialData: null,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<dynamic>?> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (snapshot.hasError) {
                          return Center(
                            child: Text("Error al cargar los pronósticos"),
                          );
                        } else if (snapshot.hasData) {
                          return ListView.builder(
                            padding: EdgeInsets.all(16),
                            itemCount: snapshot.data!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: [
                                  DatoList(snapshot.data![index], index),
                                ],
                              );
                            },
                          );
                        } else {
                          return Center(
                            child: Text("No hay datos disponibles"),
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DatoList extends StatelessWidget {
  final dynamic coment;
  final int index;

  DatoList(this.coment, this.index);

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    int currentHour = now.hour + index;

    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Color.fromARGB(255, 255, 255, 255),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Hora: ${currentHour}:00   Temperatura: ${coment.toString()}°C",
            style: const TextStyle(
              fontSize: 18,
              color: Color.fromARGB(255, 3, 3, 3),
            ),
          ),
        ],
      ),
    );
  }
}
