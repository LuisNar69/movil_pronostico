import 'dart:convert';
import 'package:app_pronostico/controls/servicio_back/RespuestaGenerica.dart';
import 'package:http/http.dart' as http;
import 'package:app_pronostico/controls/utiles/Utiles.dart';

class Conexion {
  final String URL = 'http://192.168.1.113:3006/api/';
  static bool NO_TOKEN = false;
  //news-token

  Future<RespuestaGenerica> solicitudGet(String recurso, bool token) async {
    Map<String, String> _header = {'Content-Type': 'application/json'};
    if (token == true) {
      Utiles util = Utiles();
      if (token == true) {
        _header = {'Content-Type': 'application/json', 'news-token': 'afea'};
      }
      var tokenA = await util.getValue('token');
      _header = {
        'Content-Type': 'application/json',
        'news-token': tokenA ?? ''
      };
    }

    final String _url = URL + recurso;
    final uri = Uri.parse(_url);

    try {
      final response = await http.get(uri, headers: _header);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return _response(404, "Recurso no encontrado", []);
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return _response(mapa['code'], mapa['msg'], mapa['datos']);
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return _response(mapa['code'], mapa['msg'], mapa['datos']);
      }
      // return RespuestaGenerica();
    } catch (e) {
      return _response(500, "Error Inesperado", []);
    }
  }

  Future<RespuestaGenerica> solicitudPost(
      String recurso, bool token, Map<dynamic, dynamic> data) async {
    Map<String, String> _header = {'Content-Type': 'application/json'};
    if (token == true) {
      Utiles util = Utiles();
      if (token == true) {
        _header = {'Content-Type': 'application/json', 'news-token': 'afea'};
      }
      var tokenA = await util.getValue('token');
      _header = {
        'Content-Type': 'application/json',
        'news-token': tokenA ?? ''
      };
    }

    final String _url = URL + recurso;
    final uri = Uri.parse(_url);

    try {
      final response = await http.post(uri, headers: _header);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          return _response(404, "Recurso no encontrado", []);
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          return _response(mapa['code'], mapa['msg'], mapa['datos']);
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        return _response(mapa['code'], mapa['msg'], mapa['datos']);
      }
      // return RespuestaGenerica();
    } catch (e) {
      return _response(500, "Error Inesperado", []);
    }
  }

  RespuestaGenerica _response(int code, String msg, dynamic data) {
    var respuesta = RespuestaGenerica();
    respuesta.code = code;
    respuesta.msg = msg;
    respuesta.datos = data;
    return respuesta;
  }
}
