import 'dart:developer';

import 'package:app_pronostico/controls/servicio_back/RespuestaGenerica.dart';
import 'package:app_pronostico/models/sensor.dart';

class ListaTemperaturaWS extends RespuestaGenerica {
  late Sensor data;

  ListaTemperaturaWS();

  ListaTemperaturaWS.fromMap(
      Map<dynamic, dynamic> datos, String msg, int code) {
    data = Sensor.fromMap(datos);

    this.msg = msg;
    this.code = code;
  }
}
