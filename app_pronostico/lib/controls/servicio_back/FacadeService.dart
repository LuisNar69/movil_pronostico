import 'dart:convert';
import 'dart:developer';
import 'package:app_pronostico/controls/Conexion.dart';
import 'package:app_pronostico/controls/servicio_back/modelo/ListaTemperatura.dart';
import 'package:app_pronostico/controls/utiles/Utiles.dart';
import 'package:http/http.dart' as http;

class FacadeService {
  Conexion c = Conexion();

  Future<ListaTemperaturaWS> listarDato() async {
    Utiles util = Utiles();
    //var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      //'news-token': token ?? '',
    };

    final String url = '${c.URL}sensores/temperatura';
    final uri = Uri.parse(url);

    ListaTemperaturaWS isw = ListaTemperaturaWS();

    try {
      final response = await (await (http.get(uri, headers: header)));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw = ListaTemperaturaWS.fromMap(
              {}, 'Error recurso no encontrado ', 404);
        } else {
          Map<String, dynamic> mapa = jsonDecode(response.body);
          isw = ListaTemperaturaWS.fromMap(
              {}, mapa["tag"], int.parse(mapa["code"].toString()));
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);

        Map<String, dynamic> datos = mapa["datos"];

        isw = ListaTemperaturaWS.fromMap(
            datos, mapa["tag"], int.parse(mapa["code"].toString()));
      }
    } catch (e) {
      isw = ListaTemperaturaWS.fromMap({}, 'Error $e', 500);
    }
    return isw;
  }

  Future<List<dynamic>> listarPronosticos() async {
    Utiles util = Utiles();
    //var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      // 'news-token': token ?? '',
    };

    final String url = '${c.URL}pronosticos/lista';
    final uri = Uri.parse(url);

    List<double>? isw = [];

    try {
      final response = await http.get(uri, headers: header);
      print("Respuesta de pronósticos");
      print(response.body);

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          isw = [];
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        List<dynamic> datos =
            jsonDecode(jsonEncode(mapa["temperaturasExtrapoladas"]));
        isw = datos.cast<double>();
      }
    } catch (e) {
      isw = [];
    }

    return isw ?? [];
  }
}
