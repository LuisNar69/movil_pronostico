import 'dart:developer';

class Sensor {
  String dato = '';

  Sensor();

  Sensor.fromMap(Map<dynamic, dynamic> mapa) {
    var datoRecolectado = mapa["datoRecolectado"] as List<dynamic>;
    if (datoRecolectado.isNotEmpty) {
      dato = (datoRecolectado[0]["dato"]);
    }
  }
}
